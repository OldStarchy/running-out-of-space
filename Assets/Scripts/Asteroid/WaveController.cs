﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SpawnSet
{
	public int AsteroidCount;
	public LumpyShapeGenerationParameters AsteroidType;
}
public class WaveController : MonoBehaviour
{
	public SpawnSet[] SpawnSets;
	public GameObject AsteroidPrefab;

	public WorldBounds WorldBounds;

	void Start()
	{
		foreach (var SpawnSet in SpawnSets)
		{

			for (var i = 0; i < SpawnSet.AsteroidCount; i++)
			{

				var location = new Vector2(
					Random.Range(WorldBounds.Left, WorldBounds.Right),
					Random.Range(WorldBounds.Top, WorldBounds.Bottom)
				);

				var obj = GameObject.Instantiate(AsteroidPrefab, location, Quaternion.identity);

				obj.transform.SetParent(transform);
				var ras = obj.GetComponent<LumpyShapeGenerator>();
				ras.Params = SpawnSet.AsteroidType;

				var rb = obj.GetComponent<Rigidbody2D>();
				rb.velocity = Random.insideUnitCircle * 20;
				rb.angularVelocity = Random.Range(-10f, 10);
			}
		}
	}
}
