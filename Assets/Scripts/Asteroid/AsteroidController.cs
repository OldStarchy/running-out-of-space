﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LumpyShapeGenerator), typeof(Rigidbody2D))]
public class AsteroidController : MonoBehaviour
{
	public float PopForce = 30000;

	public float MinVolume = 20;

	public float DragVelocityThreshold = 30000;
	public float FastDrag = 2;
	public float SlowDrag = 0.01f;

	public GameObject ExplosionPrefab;
	public GameObject BumpSparksPrefab;

	private Rigidbody2D _rigidbody;
	private bool _isDestroyed = false;

	[SerializeField]
	private int _health = 1;

	void Start()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
		GetComponent<LumpyShapeGenerator>().ShapeGenerated += CalculateHealth;
		CalculateHealth();
		if (MinVolume < 20)
			MinVolume = 20;
	}

	void CalculateHealth()
	{
		this._health = Mathf.CeilToInt(Mathf.Sqrt(_rigidbody.mass) / 10);
	}
	private void FixedUpdate()
	{
		if (_rigidbody.velocity.sqrMagnitude > DragVelocityThreshold)
		{
			_rigidbody.drag = FastDrag;
		}
		else
		{
			_rigidbody.drag = SlowDrag;
		}
	}

	private GameController GetGameController()
	{
		var obj = GameObject.FindWithTag("Game Controller");

		if (obj != null)
		{
			return obj.GetComponent<GameController>();
		}

		return null;
	}
	public void Destruct(bool destroySmall = false)
	{
		if (_isDestroyed)
			return;

		_health--;
		if (_health > 0 && !destroySmall)
			return;

		if (destroySmall)
		{
			_isDestroyed = true;

			if (ExplosionPrefab != null)
			{
				GameObject.Instantiate(ExplosionPrefab, transform.position, transform.rotation);
			}
			GameObject.Destroy(gameObject);

			return;
		}


		var gc = GetGameController();
		gc.Score += Mathf.RoundToInt(_rigidbody.mass / 126);

		var _lsg = GetComponent<LumpyShapeGenerator>();

		var parameters = _lsg.Params;
		var radius = (parameters.RadiusMin + parameters.RadiusMax) / 2f;
		var volume = radius * radius * Mathf.PI;
		radius *= 0.5f;

		if (volume < MinVolume)
		{
			return;
		}


		_isDestroyed = true;

		var maxBits = Mathf.CeilToInt(Mathf.Sqrt(this._rigidbody.mass) / 20);
		var bitCount = Random.Range(2, maxBits + 1);
		var displacementAngle = Random.Range(-Mathf.PI, Mathf.PI);
		var displacementAngleStep = Mathf.PI * 2 / bitCount;

		var bits = new Collider2D[bitCount];

		var newParams = ScriptableObject.CreateInstance<LumpyShapeGenerationParameters>();
		var newSize = Mathf.Sqrt((parameters.RadiusMax * parameters.RadiusMax) / bitCount) / parameters.RadiusMax;


		newParams.RadiusMin = parameters.RadiusMin * newSize;
		newParams.RadiusMax = parameters.RadiusMax * newSize;

		newParams.PointsMin = parameters.PointsMin;
		newParams.PointsMax = parameters.PointsMax;

		newParams.Jitter = parameters.Jitter * newSize;

		for (var i = 0; i < bitCount; i++)
		{
			var offset = new Vector2(
					radius * Mathf.Cos(displacementAngle),
					radius * Mathf.Sin(displacementAngle)
				);
			displacementAngle += displacementAngleStep;

			var bit = GameObject.Instantiate(
				gameObject,
				transform.position + (Vector3)offset,
				transform.rotation
			);
			bit.transform.SetParent(transform.parent);

			var lsb = bit.GetComponent<LumpyShapeGenerator>();
			lsb.Params = newParams;
			lsb.GenerateSpline();

			var rb = bit.GetComponent<Rigidbody2D>();
			rb.velocity = _rigidbody.velocity;
			rb.angularVelocity = _rigidbody.angularVelocity + Random.Range(-2f, 2);

			rb.AddForceAtPosition(offset.normalized * rb.mass * 80, transform.position, ForceMode2D.Impulse);
			bits[i] = bit.GetComponent<Collider2D>();
		}

		Physics2D.IgnoreCollision(bits[0], bits[1]);

		StartCoroutine(UnIgnoreCollision(bits[0], bits[1]));

		if (ExplosionPrefab != null)
		{
			GameObject.Instantiate(ExplosionPrefab, transform.position, transform.rotation);
		}

		Destroy(gameObject);
	}

	public static IEnumerator UnIgnoreCollision(Collider2D a, Collider2D b)
	{
		yield return new WaitForSeconds(1);
		Physics2D.IgnoreCollision(a, b, false);
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.relativeVelocity.magnitude > 50)
		{
			if (BumpSparksPrefab != null)
			{
				GameObject.Instantiate(BumpSparksPrefab);
			}
		}
	}
}
