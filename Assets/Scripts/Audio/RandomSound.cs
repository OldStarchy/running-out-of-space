﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RandomSound : MonoBehaviour
{
	public bool DestroyAfterPlaying = true;
	public AudioClip[] shoot;

	private AudioSource _audioSource;
	private AudioClip _shootClip;
	void Start()
	{
		_audioSource = gameObject.GetComponent<AudioSource>();
		Play();
	}

	public void Play()
	{
		int index = Random.Range(0, shoot.Length);
		_shootClip = shoot[index];
		_audioSource.clip = _shootClip;
		_audioSource.Play();

		Invoke("Die", _shootClip.length);
	}

	private void Die() {
		Destroy(gameObject);
	}
}
