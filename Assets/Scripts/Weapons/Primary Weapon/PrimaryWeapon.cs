﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrimaryWeapon : MonoBehaviour
{
	public KeyCode FireButton = KeyCode.Space;

	public GameObject BulletPrefab;
	public GameObject SparksPrefab;
	public float ShootForce = 2000;

	private Rigidbody2D _rigidbody;
	private Collider2D _collider;
	private GameController _gameController;
	private float _nextFireTime = 0;
	private int _maxBullets = 4;

	private bool _shoot;
	private float _fireTimeout = 0.1f;

	void Start()
	{
		_rigidbody = GetComponentInParent<Rigidbody2D>();
		_collider = GetComponentInParent<Collider2D>();

		_gameController = GameObject.FindWithTag("Game Controller").GetComponent<GameController>();

	}


	void Update()
	{
		_shoot = Input.GetKey(FireButton);
	}

	private void FixedUpdate()
	{
		if (_shoot)
		{
			Shoot();
		}
	}

	public void Shoot()
	{
		if (Time.fixedTime < _nextFireTime)
			return;

		_nextFireTime = Time.fixedTime + _fireTimeout;
		var minSpread = 5f;

		var totalSpread = (_gameController.BulletsCount - 1) * minSpread;

		var angle = -totalSpread / 2;

		for (var i = 0; i < _gameController.BulletsCount; i++)
		{
			ShootBullet(angle + Random.Range(-minSpread / 2, minSpread / 2));
			angle += minSpread;
		}
		if (null != SparksPrefab)
		{
			GameObject.Instantiate(SparksPrefab, transform.position, Quaternion.identity);
		}
	}

	public void ShootBullet(float angleOffset)
	{
		var bullet = GameObject.Instantiate(BulletPrefab, transform.position, Quaternion.identity);

		bullet.GetComponent<LumpyShapeGenerator>().GenerateSpline();

		if (_collider != null)
		{
			Physics2D.IgnoreCollision(_collider, bullet.GetComponent<Collider2D>());
		}

		var direction = (Vector2)(Quaternion.AngleAxis(angleOffset, Vector3.forward) * transform.up);

		var rb = bullet.GetComponent<Rigidbody2D>();

		if (_rigidbody != null)
		{
			rb.velocity = _rigidbody.GetPointVelocity(transform.position);
			rb.angularVelocity = _rigidbody.angularVelocity;
			_rigidbody.AddForce(-transform.up * ShootForce, ForceMode2D.Impulse);
		}

		rb.AddForce(direction * ShootForce, ForceMode2D.Impulse);
	}
}
