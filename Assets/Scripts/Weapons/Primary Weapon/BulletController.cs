﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
	private bool _hasHit = false;

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (_hasHit)
		{
			return;
		}
		_hasHit = true;
		var dest = collision.gameObject.GetComponent<AsteroidController>();

		if (null != dest)
		{
			dest.Destruct();
		}

		Destroy(gameObject);
	}
}
