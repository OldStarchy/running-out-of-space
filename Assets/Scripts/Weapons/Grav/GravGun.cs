﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravGun : MonoBehaviour
{
	public KeyCode FireButton = KeyCode.LeftControl;
	public GameObject GravBombPrefab;
	public float ShootSpeed = 30;
	public GameController GameController;

	private bool _shoot;

	private void Start()
	{
		if (GameController == null)
		{
			var obj = GameObject.FindWithTag("Game Controller");

			if (obj != null)
			{
				GameController = obj.GetComponent<GameController>();
			}
		}
	}

	void Update()
	{

		if (Input.GetKeyDown(FireButton))
		{
			_shoot = true;
		}
	}

	void FixedUpdate()
	{
		if (_shoot)
		{
			_shoot = false;
			Shoot();
		}
	}

	void Shoot()
	{
		if (GameController.BombCount > 0)
		{
			GameController.BombCount--;

			var grav = GameObject.Instantiate(GravBombPrefab, transform.position, Quaternion.identity).GetComponent<GravBombController>();
			grav.velocity = transform.up * ShootSpeed;
		}
	}
}
