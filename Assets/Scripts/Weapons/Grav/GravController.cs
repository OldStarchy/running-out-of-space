﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravController : MonoBehaviour
{
	public float Force = 30000;
	public float MaxAge = 2;

	private float _born;

	void Start()
	{
		Physics2D.IgnoreCollision(GetComponent<Collider2D>(), GetComponentInChildren<Collider2D>());
		_born = Time.fixedTime;
	}

	private void FixedUpdate()
	{
		var age = Time.fixedTime - _born;

		if (age > MaxAge)
			GameObject.Destroy(gameObject);

		var ttd = MaxAge - age;

		var scale = 1f;
		var min = Mathf.Min(age, ttd);
		if (min < 0.5)
		{
			scale = Mathf.Sin(min * Mathf.PI);
		}
		transform.localScale = Vector2.one * (
			scale * (1 +
			0.01f * Mathf.Sin((age - 0.5f) * 10) +
			0.06f * Mathf.Sin((age - 0.5f) * 4))
		);
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Asteroid"))
		{
			var orb = other.GetComponent<Rigidbody2D>();
			var dir = -(other.transform.position - transform.position);
			var dist = 100 / dir.magnitude;
			dir = dir.normalized * dist * dist;

			orb.AddForce(dir * Force);
		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Asteroid"))
		{
			var orb = other.GetComponent<Rigidbody2D>();
			orb.velocity = Vector2.zero;
		}
	}

	public void Explode()
	{
		MaxAge = Time.fixedTime + 0.5f;
	}
}
