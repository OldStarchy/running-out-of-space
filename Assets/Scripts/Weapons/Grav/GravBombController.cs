﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravBombController : MonoBehaviour
{
	public float Friction = 0.95f;
	public GameObject[] SpawnObjects;

	public Vector3 velocity { get; set; }

	void Update()
	{
		transform.position += velocity;
		velocity *= Friction;

		if (velocity.sqrMagnitude < 1)
		{
			foreach (var objPrefab in SpawnObjects)
			{
				GameObject.Instantiate(objPrefab, transform.position, Quaternion.identity);
			}

			GameObject.Destroy(gameObject);
		}
	}
}
