﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravFinCreator : MonoBehaviour
{
	public GameObject Fin;
	public int steps = 360 / 15;

	void Start()
	{
		var angleStep = 360 / steps;

		for (int i = 1; i < steps; i++)
		{
			var ob = GameObject.Instantiate(Fin, transform.position, Fin.transform.rotation);

			ob.transform.SetParent(transform);
			ob.transform.rotation *= Quaternion.AngleAxis(i * angleStep, Vector3.forward);
			ob.hideFlags |= HideFlags.HideInHierarchy;
		}
	}
}
