﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerTurretMoveController : MonoBehaviour
{
	public float RotateSpeed = 5f;

	private Rigidbody2D _rigidbody;

	void Start()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate()
	{
		var rotation = Input.GetAxis("Horizontal");

		_rigidbody.MoveRotation(_rigidbody.rotation - rotation * RotateSpeed);
	}
}
