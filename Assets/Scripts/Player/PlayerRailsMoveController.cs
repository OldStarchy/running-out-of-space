﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRailsMoveController : MonoBehaviour
{
	public float MaxSpeed = 50;
	private Rigidbody2D _rigidbody;

	void Start()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate()
	{
		var slide = Input.GetAxis("Horizontal");

		_rigidbody.MovePosition(((Vector2)transform.position) + Vector2.right * MaxSpeed * slide);
		_rigidbody.velocity *= Vector2.right;
	}
}
