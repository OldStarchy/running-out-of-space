﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerFreeMoveController : MonoBehaviour
{

	public float MaxThrust = 50000;
	public float MaxTorque = 30000;

	private Rigidbody2D _rigidbody;

	void Start()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate()
	{
		var accel = Input.GetAxis("Vertical");
		var rotat = Input.GetAxis("Horizontal");

		_rigidbody.AddForce(transform.up * MaxThrust * accel);

		_rigidbody.AddTorque(-rotat * MaxTorque);
	}
}
