﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpController : MonoBehaviour
{
	/** 0 for don't destroy */
	public int DestroyAfterLoops = 0;

	public WorldBounds rect;

	void Update()
	{

		var looped = false;
		var pos = transform.position;

		while (pos.x < rect.Left)
		{
			pos.x += rect.Width;
			looped = true;
		}

		while (pos.x > rect.Right)
		{
			pos.x -= rect.Width;
			looped = true;
		}

		while (pos.y < rect.Top)
		{
			pos.y += rect.Height;
			looped = true;
		}

		while (pos.y > rect.Bottom)
		{
			pos.y -= rect.Height;
			looped = true;
		}
		if (looped)
		{
			if (DestroyAfterLoops > 0)
			{
				DestroyAfterLoops--;

				if (DestroyAfterLoops == 0)
				{
					Destroy(gameObject);
					return;
				}
			}

			transform.position = pos;
		}
	}
}
