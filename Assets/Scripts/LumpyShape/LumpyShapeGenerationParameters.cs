﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AsteroidType", menuName = "AsteroidType", order = 1)]
public class LumpyShapeGenerationParameters : ScriptableObject {
	public float RadiusMin = 10;
	public float RadiusMax = 20;

	public int PointsMin = 5;
	public int PointsMax = 8;

	public float Jitter = 1;

	public LumpyShapeGenerationParameters Next;
}
