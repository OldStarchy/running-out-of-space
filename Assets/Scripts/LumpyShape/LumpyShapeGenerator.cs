﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RageSpline))]
public class LumpyShapeGenerator : MonoBehaviour
{
	public LumpyShapeGenerationParameters Params;

	private bool _hasGeneratedSpline = false;

	void Start()
	{
		if (!_hasGeneratedSpline)
			GenerateSpline();
	}

	public delegate void ShapeGeneratedEvent();
	public event ShapeGeneratedEvent ShapeGenerated;

	public void GenerateSpline()
	{
		var spline = GetComponent<RageSpline>();

		_hasGeneratedSpline = true;

		var radius = Random.Range(Params.RadiusMin, Params.RadiusMax);
		var points = Random.Range(Params.PointsMin, Params.PointsMax);

		spline.ClearPoints();

		var pi4 = Mathf.PI / 4f;
		var pi2 = Mathf.PI / 2f;

		var step = 2 * Mathf.PI / points;

		var angle = Random.Range(0f, 360f);

		for (var i = 0; i < points; i++)
		{
			var location = new Vector2(
				radius * Mathf.Cos(angle),
				radius * Mathf.Sin(angle)
			);
			var tangent = angle - pi2;

			if (Params.Jitter > 0)
			{
				location = location + (Random.insideUnitCircle * Params.Jitter);
				tangent += Random.Range(-pi4, pi4);
			}

			spline.AddPoint(
				0,
				location,
				new Vector3(
					Mathf.Cos(tangent),
					Mathf.Sin(tangent)
				) * location.magnitude * step * 0.4f
			);

			angle += step;
		}

		spline.RefreshMesh();

		if (ShapeGenerated != null)
			ShapeGenerated.Invoke();
	}
}
