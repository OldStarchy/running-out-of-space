﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
	public Text RestartText;
	public GameObject GravPrefab;


	public int BulletsCount = 1;

	public GameObject BGM;

	public GameObject NewLifeSoundPrefab;
	public GameObject NewBombSoundPrefab;
	public int StartingBombCount = 2;
	public delegate void GameStatChange(int oldValue, int newValue);
	private int _bombCount = 2;
	public int BombCount
	{
		get { return _bombCount; }
		set
		{
			if (_bombCount != value)
			{
				if (OnBombCountChange != null)
				{
					OnBombCountChange.Invoke(_bombCount, value);
					_bombCount = value;
				}
			}
		}
	}
	public event GameStatChange OnBombCountChange;
	private int _score = 0;
	public int Score
	{
		get { return _score; }
		set
		{
			if (_score != value)
			{
				if (OnScoreChange != null)
				{
					OnScoreChange.Invoke(_score, value);
					_score = value;
				}
			}
		}
	}
	public int StartingExtraLives = 1;
	public event GameStatChange OnScoreChange;
	private int _extraLives = 1;
	public int ExtraLives
	{
		get { return _extraLives; }
		set
		{
			if (_extraLives != value)
			{
				if (OnExtraLivesChange != null)
				{
					OnExtraLivesChange.Invoke(_extraLives, value);
					_extraLives = value;
				}
			}
		}
	}
	public event GameStatChange OnExtraLivesChange;

	private int _lastBombAt = 0;
	private int _lastLifeAt = 0;
	// Use this for initialization
	void Start()
	{
		BombCount = StartingBombCount;
		ExtraLives = StartingExtraLives;
		RestartText.gameObject.SetActive(false);


		OnScoreChange += (int o, int n) =>
		{
			if (n > _lastBombAt + 2000)
			{
				_lastBombAt = _lastBombAt + 2000;
				BombCount++;
				GameObject.Instantiate(NewBombSoundPrefab);
			}

			if (n > _lastLifeAt + 5000)
			{
				_lastLifeAt = _lastLifeAt + 5000;
				ExtraLives++;
				GameObject.Instantiate(NewLifeSoundPrefab);
			}

			BulletsCount = Mathf.Min(Mathf.CeilToInt(n / 200f), 5);
		};
	}

	void Restart()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
		}

		if (Input.GetKeyDown(KeyCode.Space) && ExtraLives < 0)
		{
			Restart();
		}

		if (Input.GetKeyDown(KeyCode.M))
		{
			BGM.SetActive(!BGM.activeSelf);
		}
	}

	public void RespawnPlayer(GameObject player)
	{
		StartCoroutine(Respawn(player));
	}

	IEnumerator Respawn(GameObject player)
	{
		// Don't hate me its late and I want to finish this
		GameObject.Instantiate(GravPrefab, player.transform.position, Quaternion.identity).GetComponent<Rigidbody2D>().velocity = Vector2.up * 100;

		player.SetActive(false);

		yield return new WaitForSeconds(1);

		ExtraLives--;

		if (ExtraLives >= 0)
		{
			yield return new WaitForSeconds(1);

			player.SetActive(true);
		}
		else
		{
			RestartText.gameObject.SetActive(true);
		}
	}
}
