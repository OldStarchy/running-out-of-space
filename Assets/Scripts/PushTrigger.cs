﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class PushTrigger : MonoBehaviour
{
	public Vector2 Force = Vector2.down * 30000;
	private BoxCollider2D _box;

	void Start()
	{
		_box = GetComponent<BoxCollider2D>();
	}
	private void OnTriggerStay2D(Collider2D other)
	{
		if (other.CompareTag("Asteroid"))
		{
			var rb = other.GetComponent<Rigidbody2D>();

			if (rb == null)
				return;

			rb.AddForce(Force);
		}
	}

	private void OnDrawGizmos()
	{
		var _box = GetComponent<BoxCollider2D>();
		var center = (Vector2)transform.position + _box.offset;
		var tl = center - _box.size / 2;
		var br = center + _box.size / 2;
		var tr = new Vector2(br.x, tl.y);
		var bl = new Vector2(tl.x, br.y);
		Debug.DrawLine(tl, tr, Color.magenta);
		Debug.DrawLine(bl, br, Color.magenta);
		Debug.DrawLine(tl, bl, Color.magenta);
		Debug.DrawLine(tr, br, Color.magenta);

		Debug.DrawLine(transform.position, transform.position + (Vector3)(Force / 300), Color.magenta);
	}
}
