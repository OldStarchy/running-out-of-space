﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WorldBounds", menuName = "WorldBounds", order = 1)]
public class WorldBounds : ScriptableObject
{
	public float Top = -370;
	public float Left = -600;
	public float Right = 600;
	public float Bottom = 370;

	public float Width { get { return Right - Left; } }
	public float Height { get { return Bottom - Top; } }
}
