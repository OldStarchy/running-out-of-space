﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
	public float Speed = 0.1f;

	void Start()
	{
		transform.rotation = Quaternion.AngleAxis(Random.Range(0, 360f), Vector3.forward);
	}

	void Update()
	{
		transform.rotation *= Quaternion.AngleAxis(Speed, Vector3.forward);
	}
}
