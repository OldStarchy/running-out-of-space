﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deleter : MonoBehaviour
{
	public string[] Tags;

	private void OnTriggerEnter2D(Collider2D other)
	{
		foreach (var tag in Tags)
		{
			if (other.gameObject.CompareTag(tag))
				GameObject.Destroy(other.gameObject);
			return;
		}
	}
	private void OnDrawGizmos()
	{
		var _box = GetComponent<BoxCollider2D>();
		var center = (Vector2)transform.position + _box.offset;
		var tl = center - _box.size / 2;
		var br = center + _box.size / 2;
		var tr = new Vector2(br.x, tl.y);
		var bl = new Vector2(tl.x, br.y);
		Debug.DrawLine(tl, tr, Color.red);
		Debug.DrawLine(bl, br, Color.red);
		Debug.DrawLine(tl, bl, Color.red);
		Debug.DrawLine(tr, br, Color.red);
	}
}
