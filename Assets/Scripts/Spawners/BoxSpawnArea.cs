﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class BoxSpawnArea : SpawnArea
{
	private BoxCollider2D _box;

	void Start()
	{
		_box = GetComponent<BoxCollider2D>();
	}

	public override Vector2 GetSpawnPoint()
	{
		var halfSize = _box.size * 0.5f;
		return new Vector2(
					Random.Range(-halfSize.x, halfSize.x),
					Random.Range(-halfSize.y, halfSize.y)
				) + (Vector2)transform.position;
	}

	public GameObject SpawnPrefab(GameObject prefab)
	{
		return GameObject.Instantiate(prefab, GetSpawnPoint(), Quaternion.identity);
	}

	private void OnDrawGizmos()
	{
		var _box = GetComponent<BoxCollider2D>();
		var center = (Vector2)transform.position + _box.offset;
		var tl = center - _box.size / 2;
		var br = center + _box.size / 2;
		var tr = new Vector2(br.x, tl.y);
		var bl = new Vector2(tl.x, br.y);
		Debug.DrawLine(tl, tr, Color.green);
		Debug.DrawLine(bl, br, Color.green);
		Debug.DrawLine(tl, bl, Color.green);
		Debug.DrawLine(tr, br, Color.green);
	}
}
