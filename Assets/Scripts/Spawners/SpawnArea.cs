﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpawnArea : MonoBehaviour
{

	public abstract Vector2 GetSpawnPoint();

}
