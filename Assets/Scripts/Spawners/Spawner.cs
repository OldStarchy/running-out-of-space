﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
	public SpawnArea SpawnArea;
	public GameObject ObjectPrefab;
	public Transform Parent;

	void Start()
	{
		SpawnArea = SpawnArea ?? GetComponent<SpawnArea>();
		if (Parent == null)
		{
			var go = GameObject.FindGameObjectWithTag("Asteroid Parent");
			if (go != null)
				Parent = go.transform;
		}

		if (SpawnArea == null)
		{
			Debug.LogError("Spawner doesn't have a spawn area set");
			enabled = false;
		}

		StartCoroutine(SpawnTimeline());
	}

	IEnumerator SpawnTimeline()
	{
		while (true)
		{
			yield return new WaitForSeconds(1);
			SpawnPrefab(ObjectPrefab);
		}
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			SpawnPrefab(ObjectPrefab);
		}
	}

	public GameObject SpawnPrefab(GameObject prefab)
	{
		var go = GameObject.Instantiate(prefab, SpawnArea.GetSpawnPoint(), Quaternion.identity);
		if (Parent != null)
			go.transform.SetParent(Parent);
		return go;
	}
}
