﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombReadout : MonoBehaviour
{
	public GameObject MarkerPrefab;
	public GameController GameController;

	public Vector2 Offset = Vector2.right * 20;

	void Start()
	{
		GameController.OnBombCountChange += UpdateReadout;

		UpdateReadout(0, GameController.BombCount);
	}

	void UpdateReadout(int oldBombs, int newBombs)
	{
		foreach (Transform child in transform)
		{
			GameObject.Destroy(child.gameObject);
		}

		for (var i = 0; i < newBombs; i++)
		{
			Transform marker = null;
			marker = GameObject.Instantiate(MarkerPrefab).transform;
			marker.SetParent(transform);
			marker.transform.position = transform.position + (Vector3)(Offset * i);
		}
	}
}
