﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivesReadout : MonoBehaviour
{
	public GameObject MarkerPrefab;
	public GameController GameController;

	public Vector2 Offset = Vector2.left * 20;

	void Start()
	{
		GameController.OnExtraLivesChange += UpdateReadout;

		UpdateReadout(0, GameController.ExtraLives);
	}

	void UpdateReadout(int oldLives, int newLives)
	{
		foreach (Transform child in transform)
		{
			GameObject.Destroy(child.gameObject);
		}

		for (var i = 0; i < newLives; i++)
		{
			Transform marker = null;
			marker = GameObject.Instantiate(MarkerPrefab).transform;
			marker.SetParent(transform);
			marker.transform.position = transform.position + (Vector3)(Offset * i);
		}

	}
}
