﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveMeAfterSomeTime : MonoBehaviour
{
	void Start()
	{
		Invoke("Die", 5);
	}

	void Die()
	{
		Destroy(gameObject);
	}
}
