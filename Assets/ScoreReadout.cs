﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreReadout : MonoBehaviour
{
	private GameController _gameController;

	void Start()
	{
		var obj = GameObject.FindWithTag("Game Controller");

		if (obj != null)
		{
			_gameController = obj.GetComponent<GameController>();
		}

		if (_gameController != null)
		{
			_gameController.OnScoreChange += UpdateReadout;
		}

		UpdateReadout(0, 0);
	}

	private void UpdateReadout(int oldScore, int newScore)
	{
		this.GetComponent<UnityEngine.UI.Text>().text = newScore.ToString();
	}
}
