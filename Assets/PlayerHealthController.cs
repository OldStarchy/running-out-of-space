﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthController : MonoBehaviour
{
	public GameController GameController;
	public int StartHealth = 10;
	private int _health = 10;

	private void Start()
	{
		_health = StartHealth;
		if (GameController == null)
		{
			var obj = GameObject.FindWithTag("Game Controller");

			if (obj != null)
			{
				GameController = obj.GetComponent<GameController>();
			}
		}
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Asteroid"))
		{
			if (other.rigidbody.mass < 30)
				return;

			if (other.relativeVelocity.magnitude > 50)
			{
				_health -= Mathf.RoundToInt((other.relativeVelocity.magnitude - 50) / 300);

				if (_health <= 0)
				{
					GameController.RespawnPlayer(gameObject);
					_health = StartHealth;
				}
			}
		}
	}
}
