# Ludum Dare 42 "Running out of space"

I finished _something_ which means I've completed my goal.

The game is not great.
The code is not great.

But, I did learn about a lot of things while making this :).

## Details

Developed in Unity 2018.2.1f1 Personal
Sound effects by SFXR
Background music from https://freesound.org
Background image from royalty free image search on Google

## Gameplay

Asteroids fall from the top of the screen. Shoot your way through, but don't get hit by the falling debris.

Move - Left / Right
Shoot - Space - You start with one "barrel" and it goes up to 5 as your score increases
Grav Bomb - Left Control - You start with 2, and get a new one every 2000 points
Extra Lives - You start with 1, and get a new one every 5000 points
Health - Your ship has health, you take damage by hitting asteroids. The harder you hit, the more damage.
Score - You get points by destroying asteroids. You don't get points for using the Grav Bomb though.
Asteroids - You can't destroy the small ones, you just have to avoid them.

If you get sick of the short music loop you can turn toggle it with "M".
